package net.ihe.gazelle.chxds.validator.chxds;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.LocalizedStringType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;


 /**
  * class :        AdhocQueryConstraints
  * package :   chxds
  * Constraint Spec Class
  * class of test : AdhocQueryType
  * 
  */
public final class AdhocQueryConstraintsValidator{


    private AdhocQueryConstraintsValidator() {}



	/**
	* Validation of instance by a constraint : chxds_metadatalevel
	* The parameter$MetadataLevel, whenever provided, shall equal to 1 (one).
	*
	*/
	private static boolean _validateAdhocQueryConstraints_Chxds_metadatalevel(net.ihe.gazelle.rim.AdhocQueryType aClass){
		java.util.ArrayList<SlotType1> result2;
		result2 = new java.util.ArrayList<SlotType1>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (SlotType1 anElement1 : aClass.getSlot()) {
			    if ((!(((String) anElement1.getName()) == null) && anElement1.getName().equals("$MetadataLevel"))) {
			        result2.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (SlotType1 anElement2 : result2) {
			    Boolean result3;
			    result3 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (String anElement3 : anElement2.getValueList().getValue()) {
			    	    if (!anElement3.equals("1")) {
			    	        result3 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!(!(anElement2.getValueList() == null) && result3)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : AdhocQueryConstraints
    * Verify if an element of type AdhocQueryConstraints can be validated by AdhocQueryConstraints
	*
	*/
	public static boolean _isAdhocQueryConstraints(net.ihe.gazelle.rim.AdhocQueryType aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   AdhocQueryConstraints
     * class ::  net.ihe.gazelle.rim.AdhocQueryType
     * 
     */
    public static void _validateAdhocQueryConstraints(net.ihe.gazelle.rim.AdhocQueryType aClass, String location, List<Notification> diagnostic) {
		if (_isAdhocQueryConstraints(aClass)){
			executeCons_AdhocQueryConstraints_Chxds_metadatalevel(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AdhocQueryConstraints_Chxds_metadatalevel(net.ihe.gazelle.rim.AdhocQueryType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAdhocQueryConstraints_Chxds_metadatalevel(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("chxds_metadatalevel");
		notif.setDescription("The parameter$MetadataLevel, whenever provided, shall equal to 1 (one).");
		notif.setLocation(location);
		notif.setIdentifiant("chxds-AdhocQueryConstraints-chxds_metadatalevel");
		
		diagnostic.add(notif);
	}

}
