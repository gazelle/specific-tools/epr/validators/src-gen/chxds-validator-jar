package net.ihe.gazelle.chxds.validator.chxdsdsdr;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.RegistryPackageType;



 /**
  * class :        CHXDSDRDocumentEntry
  * package :   chxdsdsdr
  * Constraint Spec Class
  * class of test : ExtrinsicObjectType
  * 
  */
public final class CHXDSDRDocumentEntryValidator{


    private CHXDSDRDocumentEntryValidator() {}



	/**
	* Validation of instance by a constraint : chxds_documentEntryTitle
	* DocumentEntry.title is required.
	*
	*/
	private static boolean _validateCHXDSDRDocumentEntry_Chxds_documentEntryTitle(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		return (!(aClass.getName() == null) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(aClass.getName().getLocalizedString()) > new Integer(0)));
		
	}

	/**
	* Validation of class-constraint : CHXDSDRDocumentEntry
    * Verify if an element of type CHXDSDRDocumentEntry can be validated by CHXDSDRDocumentEntry
	*
	*/
	public static boolean _isCHXDSDRDocumentEntry(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHXDSDRDocumentEntry
     * class ::  net.ihe.gazelle.rim.ExtrinsicObjectType
     * 
     */
    public static void _validateCHXDSDRDocumentEntry(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, String location, List<Notification> diagnostic) {
		if (_isCHXDSDRDocumentEntry(aClass)){
			executeCons_CHXDSDRDocumentEntry_Chxds_documentEntryTitle(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHXDSDRDocumentEntry_Chxds_documentEntryTitle(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXDSDRDocumentEntry_Chxds_documentEntryTitle(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("chxds_documentEntryTitle");
		notif.setDescription("DocumentEntry.title is required.");
		notif.setLocation(location);
		notif.setIdentifiant("chxdsdsdr-CHXDSDRDocumentEntry-chxds_documentEntryTitle");
		
		diagnostic.add(notif);
	}

}
