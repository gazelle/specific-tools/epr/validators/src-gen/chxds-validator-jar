package net.ihe.gazelle.chxds.validator.chxdsdsdr;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.RegistryPackageType;



 /**
  * class :        CHXDSDSDRConstraint
  * package :   chxdsdsdr
  * Constraint Spec Class
  * class of test : RegistryObjectListType
  * 
  */
public final class CHXDSDSDRConstraintValidator{


    private CHXDSDSDRConstraintValidator() {}



	/**
	* Validation of instance by a constraint : chxds_submissionSetAuthor
	* SubmissionSet.Author is required
	*
	*/
	private static boolean _validateCHXDSDSDRConstraint_Chxds_submissionSetAuthor(net.ihe.gazelle.rim.RegistryObjectListType aClass){
		java.util.ArrayList<RegistryPackageType> result2;
		result2 = new java.util.ArrayList<RegistryPackageType>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (RegistryPackageType anElement1 : aClass.getRegistryPackage()) {
			    java.util.ArrayList<ClassificationType> result3;
			    result3 = new java.util.ArrayList<ClassificationType>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (ClassificationType anElement2 : anElement1.getClassification()) {
			    	    if ((((!(((String) anElement2.getClassificationNode()) == null) && anElement2.getClassificationNode().equals("urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd")) && !(((String) anElement2.getClassifiedObject()) == null)) && anElement2.getClassifiedObject().equals(anElement1.getId()))) {
			    	        result3.add(anElement2);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    java.util.ArrayList<ClassificationType> result4;
			    result4 = new java.util.ArrayList<ClassificationType>();
			    
			    /* Iterator Select: Select all elements which fulfill the condition. */
			    try{
			    	for (ClassificationType anElement3 : aClass.getClassification()) {
			    	    if ((((!(((String) anElement3.getClassificationNode()) == null) && anElement3.getClassificationNode().equals("urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd")) && !(((String) anElement3.getClassifiedObject()) == null)) && anElement3.getClassifiedObject().equals(anElement1.getId()))) {
			    	        result4.add(anElement3);
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result3) > new Integer(0)) || (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result4) > new Integer(0)))) {
			        result2.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (RegistryPackageType anElement4 : result2) {
			    Boolean result5;
			    result5 = false;
			    
			    /* Iterator Exists: Iterate and check, if any element fulfills the condition. */
			    try{
			    	for (ClassificationType anElement5 : anElement4.getClassification()) {
			    	    if ((!(((String) anElement5.getClassificationScheme()) == null) && anElement5.getClassificationScheme().equals("urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d"))) {
			    	        result5 = true;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!result5) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of class-constraint : CHXDSDSDRConstraint
    * Verify if an element of type CHXDSDSDRConstraint can be validated by CHXDSDSDRConstraint
	*
	*/
	public static boolean _isCHXDSDSDRConstraint(net.ihe.gazelle.rim.RegistryObjectListType aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHXDSDSDRConstraint
     * class ::  net.ihe.gazelle.rim.RegistryObjectListType
     * 
     */
    public static void _validateCHXDSDSDRConstraint(net.ihe.gazelle.rim.RegistryObjectListType aClass, String location, List<Notification> diagnostic) {
		if (_isCHXDSDSDRConstraint(aClass)){
			executeCons_CHXDSDSDRConstraint_Chxds_submissionSetAuthor(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHXDSDSDRConstraint_Chxds_submissionSetAuthor(net.ihe.gazelle.rim.RegistryObjectListType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXDSDSDRConstraint_Chxds_submissionSetAuthor(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("chxds_submissionSetAuthor");
		notif.setDescription("SubmissionSet.Author is required");
		notif.setLocation(location);
		notif.setIdentifiant("chxdsdsdr-CHXDSDSDRConstraint-chxds_submissionSetAuthor");
		
		diagnostic.add(notif);
	}

}
