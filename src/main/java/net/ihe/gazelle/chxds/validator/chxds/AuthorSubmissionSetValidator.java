package net.ihe.gazelle.chxds.validator.chxds;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;


 /**
  * class :        AuthorSubmissionSet
  * package :   chxds
  * Template Class
  * Template identifier : urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d
  * Class of test : ClassificationType
  * 
  */
public final class AuthorSubmissionSetValidator{


    private AuthorSubmissionSetValidator() {}



	/**
	* Validation of instance by a constraint : CHXDS_Author_AuthorRole_In_ValueSet
	* If present, the value of the AuthorRole attribute SHALL be taken from the SubmissionSet.Author.AuthorRole value set with the OID 2.16.756.5.30.1.127.3.10.1.41
	*
	*/
	private static boolean _validateAuthorSubmissionSet_CHXDS_Author_AuthorRole_In_ValueSet(net.ihe.gazelle.rim.ClassificationType aClass){
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (SlotType1 anElement1 : aClass.getSlot()) {
			    Boolean result2;
			    result2 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (String anElement2 : anElement1.getValueList().getValue()) {
			    	    if (!anElement1.matchesValueSet("2.16.756.5.30.1.127.3.10.1.41", tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclSequences.at(tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclBags.asSequence(anElement1.getValueList().extractCodeFromAdhocValue(anElement2)), new Integer(1)), tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclSequences.at(tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclBags.asSequence(anElement1.getValueList().extractCodeFromAdhocValue(anElement2)), new Integer(2)), null, null)) {
			    	        result2 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!(!(!(((String) anElement1.getName()) == null) && anElement1.getName().equals("authorRole")) || (!(anElement1.getValueList() == null) && result2))) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of template-constraint by a constraint : AuthorSubmissionSet
    * Verify if an element can be token as a Template of type AuthorSubmissionSet
	*
	*/
	private static boolean _isAuthorSubmissionSetTemplate(net.ihe.gazelle.rim.ClassificationType aClass){
		return (!(((String) aClass.getClassificationScheme()) == null) && aClass.getClassificationScheme().equals("urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d"));
				
	}
	/**
	* Validation of class-constraint : AuthorSubmissionSet
    * Verify if an element of type AuthorSubmissionSet can be validated by AuthorSubmissionSet
	*
	*/
	public static boolean _isAuthorSubmissionSet(net.ihe.gazelle.rim.ClassificationType aClass){
		return _isAuthorSubmissionSetTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   AuthorSubmissionSet
     * class ::  net.ihe.gazelle.rim.ClassificationType
     * 
     */
    public static void _validateAuthorSubmissionSet(net.ihe.gazelle.rim.ClassificationType aClass, String location, List<Notification> diagnostic) {
		if (_isAuthorSubmissionSet(aClass)){
			executeCons_AuthorSubmissionSet_CHXDS_Author_AuthorRole_In_ValueSet(aClass, location, diagnostic);
		}
	}

	private static void executeCons_AuthorSubmissionSet_CHXDS_Author_AuthorRole_In_ValueSet(net.ihe.gazelle.rim.ClassificationType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateAuthorSubmissionSet_CHXDS_Author_AuthorRole_In_ValueSet(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("CHXDS_Author_AuthorRole_In_ValueSet");
		notif.setDescription("If present, the value of the AuthorRole attribute SHALL be taken from the SubmissionSet.Author.AuthorRole value set with the OID 2.16.756.5.30.1.127.3.10.1.41");
		notif.setLocation(location);
		notif.setIdentifiant("chxds-AuthorSubmissionSet-CHXDS_Author_AuthorRole_In_ValueSet");
		
		diagnostic.add(notif);
	}

}
