package net.ihe.gazelle.chxds.validator.chxds;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;


 /**
  * class :        DocumentEntryOriginalProviderRole
  * package :   chxds
  * Template Class
  * Template identifier : urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1
  * Class of test : ExtrinsicObjectType
  * 
  */
public final class DocumentEntryOriginalProviderRoleValidator{


    private DocumentEntryOriginalProviderRoleValidator() {}



	/**
	* Validation of instance by a constraint : CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet
	* Values SHALL be taken from the value set DocumentEntry.originalProviderRole (OID: 2.16.756.5.30.1.127.3.10.1.42).
	*
	*/
	private static boolean _validateDocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		java.util.ArrayList<SlotType1> result1;
		result1 = new java.util.ArrayList<SlotType1>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (SlotType1 anElement1 : aClass.getSlot()) {
			    if ((!(((String) anElement1.getName()) == null) && anElement1.getName().equals("urn:e-health-suisse:2020:originalProviderRole"))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		java.util.ArrayList<SlotType1> result3;
		result3 = new java.util.ArrayList<SlotType1>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (SlotType1 anElement2 : aClass.getSlot()) {
			    if ((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:e-health-suisse:2020:originalProviderRole"))) {
			        result3.add(anElement2);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		Boolean result2;
		result2 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (SlotType1 anElement3 : result3) {
			    Boolean result4;
			    result4 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (String anElement4 : anElement3.getValueList().getValue()) {
			    	    if (!anElement3.matchesValueSet("2.16.756.5.30.1.127.3.10.1.42", tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclSequences.at(tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclBags.asSequence(anElement3.getValueList().extractCodeFromAdhocValue(anElement4)), new Integer(1)), tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclSequences.at(tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclBags.asSequence(anElement3.getValueList().extractCodeFromAdhocValue(anElement4)), new Integer(2)), null, null)) {
			    	        result4 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!(!(anElement3.getValueList() == null) && result4)) {
			        result2 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0)) && result2);
		
		
	}

	/**
	* Validation of instance by a constraint : CHXDS_DocumentEntry_OriginalProviderRole_Present
	* The extra metadata attribute DocumentEntry.originalProviderRole SHALL be used.
	*
	*/
	private static boolean _validateDocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_Present(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		java.util.ArrayList<SlotType1> result2;
		result2 = new java.util.ArrayList<SlotType1>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (SlotType1 anElement1 : aClass.getSlot()) {
			    if ((!(((String) anElement1.getName()) == null) && anElement1.getName().equals("urn:e-health-suisse:2020:originalProviderRole"))) {
			        result2.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		Boolean result1;
		result1 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (SlotType1 anElement2 : result2) {
			    Boolean result3;
			    result3 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (String anElement3 : anElement2.getValueList().getValue()) {
			    	    java.util.Set<String> result4;
			    	    result4 = new java.util.HashSet<String>();
			    	    result4.add(anElement3);
			    	    if (!(tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result4) > new Integer(0))) {
			    	        result3 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!(!(anElement2.getValueList() == null) || result3)) {
			        result1 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return result1;
		
		
	}

	/**
	* Validation of instance by a constraint : CHXDS_DocumentEntry_OriginalProviderRole_isCX
	* Values SHALL be formatted as Coded String data type defined in ITI TF 3, Table 4.2.3.1.7-2, i.e. as Code^^^&CodeSystemID&ISO.
	*
	*/
	private static boolean _validateDocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_isCX(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		java.util.ArrayList<SlotType1> result1;
		result1 = new java.util.ArrayList<SlotType1>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (SlotType1 anElement1 : aClass.getSlot()) {
			    if ((!(((String) anElement1.getName()) == null) && anElement1.getName().equals("urn:e-health-suisse:2020:originalProviderRole"))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		java.util.ArrayList<SlotType1> result3;
		result3 = new java.util.ArrayList<SlotType1>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (SlotType1 anElement2 : aClass.getSlot()) {
			    if ((!(((String) anElement2.getName()) == null) && anElement2.getName().equals("urn:e-health-suisse:2020:originalProviderRole"))) {
			        result3.add(anElement2);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		Boolean result2;
		result2 = true;
		
		/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
		try{
			for (SlotType1 anElement3 : result3) {
			    Boolean result4;
			    result4 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (String anElement4 : anElement3.getValueList().getValue()) {
			    	    if (!anElement3.isCX_XDS(anElement4)) {
			    	        result4 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}
		
			    if (!(!(anElement3.getValueList() == null) && result4)) {
			        result2 = false;
			        break;
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return ((tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0)) && result2);
		
		
	}

	/**
	* Validation of template-constraint by a constraint : DocumentEntryOriginalProviderRole
    * Verify if an element can be token as a Template of type DocumentEntryOriginalProviderRole
	*
	*/
	private static boolean _isDocumentEntryOriginalProviderRoleTemplate(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		return (!(aClass.getObjectType() == null) && aClass.getObjectType().equals("urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1"));
				
	}
	/**
	* Validation of class-constraint : DocumentEntryOriginalProviderRole
    * Verify if an element of type DocumentEntryOriginalProviderRole can be validated by DocumentEntryOriginalProviderRole
	*
	*/
	public static boolean _isDocumentEntryOriginalProviderRole(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		return _isDocumentEntryOriginalProviderRoleTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   DocumentEntryOriginalProviderRole
     * class ::  net.ihe.gazelle.rim.ExtrinsicObjectType
     * 
     */
    public static void _validateDocumentEntryOriginalProviderRole(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, String location, List<Notification> diagnostic) {
		if (_isDocumentEntryOriginalProviderRole(aClass)){
			executeCons_DocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet(aClass, location, diagnostic);
			executeCons_DocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_Present(aClass, location, diagnostic);
			executeCons_DocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_isCX(aClass, location, diagnostic);
		}
	}

	private static void executeCons_DocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateDocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet");
		notif.setDescription("Values SHALL be taken from the value set DocumentEntry.originalProviderRole (OID: 2.16.756.5.30.1.127.3.10.1.42).");
		notif.setLocation(location);
		notif.setIdentifiant("chxds-DocumentEntryOriginalProviderRole-CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet");
		
		diagnostic.add(notif);
	}

	private static void executeCons_DocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_Present(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateDocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_Present(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("CHXDS_DocumentEntry_OriginalProviderRole_Present");
		notif.setDescription("The extra metadata attribute DocumentEntry.originalProviderRole SHALL be used.");
		notif.setLocation(location);
		notif.setIdentifiant("chxds-DocumentEntryOriginalProviderRole-CHXDS_DocumentEntry_OriginalProviderRole_Present");
		
		diagnostic.add(notif);
	}

	private static void executeCons_DocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_isCX(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateDocumentEntryOriginalProviderRole_CHXDS_DocumentEntry_OriginalProviderRole_isCX(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("CHXDS_DocumentEntry_OriginalProviderRole_isCX");
		notif.setDescription("Values SHALL be formatted as Coded String data type defined in ITI TF 3, Table 4.2.3.1.7-2, i.e. as Code^^^&CodeSystemID&ISO.");
		notif.setLocation(location);
		notif.setIdentifiant("chxds-DocumentEntryOriginalProviderRole-CHXDS_DocumentEntry_OriginalProviderRole_isCX");
		
		diagnostic.add(notif);
	}

}
