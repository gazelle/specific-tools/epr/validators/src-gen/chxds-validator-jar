package net.ihe.gazelle.chxds.validator.chxds;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Assertion;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.LocalizedStringType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;


 /**
  * class :        DocumentEntryDicomConstraints
  * package :   chxds
  * Template Class
  * Template identifier : 
  * Class of test : ExtrinsicObjectType
  * 
  */
public final class DocumentEntryDicomConstraintsValidator{


    private DocumentEntryDicomConstraintsValidator() {}



	/**
	* Validation of instance by a constraint : CHFormatCodeIsDICOMManifestConstraint
	* When Document Sources provide KOS objects in Provide and Register Document Set, DocumentEntry.formatCode SHALL be equal to the coded value of “DICOM Manifest” (“1.2.840.10008.5.1.4.1.1.88.59^^1.2.840.10008.2.6.1”).
	*
	*/
	private static boolean _validateDocumentEntryDicomConstraints_CHFormatCodeIsDICOMManifestConstraint(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		java.util.ArrayList<ClassificationType> result1;
		result1 = new java.util.ArrayList<ClassificationType>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (ClassificationType anElement1 : aClass.getClassification()) {
			    Boolean result2;
			    result2 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (SlotType1 anElement2 : anElement1.getSlot()) {
			    	    Boolean result3;
			    	    result3 = true;
			    	    
			    	    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    	    try{
			    	    	for (String anElement3 : anElement2.getValueList().getValue()) {
			    	    	    if (!anElement3.equals("1.2.840.10008.2.6.1")) {
			    	    	        result3 = false;
			    	    	        break;
			    	    	    }
			    	    	    // no else
			    	    	}
			    	    }
			    	    catch(Exception e){}
			    
			    	    if (!result3) {
			    	        result2 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}

			    if (((((!(((String) anElement1.getClassificationScheme()) == null) && anElement1.getClassificationScheme().equals("urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d")) && anElement1.getNodeRepresentation().equals("1.2.840.10008.5.1.4.1.1.88.59")) && result2))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0));
		
		
	}

	/**
	* Validation of template-constraint by a constraint : DocumentEntryDicomConstraints
    * Verify if an element can be token as a Template of type DocumentEntryDicomConstraints
	*
	*/
	private static boolean _isDocumentEntryDicomConstraintsTemplate(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		return ((!(((String) aClass.getMimeType()) == null) && !aClass.getMimeType().equals("")) && aClass.getMimeType().equals("application/dicom"));
				
	}
	/**
	* Validation of class-constraint : DocumentEntryDicomConstraints
    * Verify if an element of type DocumentEntryDicomConstraints can be validated by DocumentEntryDicomConstraints
	*
	*/
	public static boolean _isDocumentEntryDicomConstraints(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		return _isDocumentEntryDicomConstraintsTemplate(aClass);
	}

/**
     * Validate as a template
     * name ::   DocumentEntryDicomConstraints
     * class ::  net.ihe.gazelle.rim.ExtrinsicObjectType
     * 
     */
    public static void _validateDocumentEntryDicomConstraints(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, String location, List<Notification> diagnostic) {
		if (_isDocumentEntryDicomConstraints(aClass)){
			executeCons_DocumentEntryDicomConstraints_CHFormatCodeIsDICOMManifestConstraint(aClass, location, diagnostic);
		}
	}

	private static void executeCons_DocumentEntryDicomConstraints_CHFormatCodeIsDICOMManifestConstraint(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateDocumentEntryDicomConstraints_CHFormatCodeIsDICOMManifestConstraint(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("CHFormatCodeIsDICOMManifestConstraint");
		notif.setDescription("When Document Sources provide KOS objects in Provide and Register Document Set, DocumentEntry.formatCode SHALL be equal to the coded value of “DICOM Manifest” (“1.2.840.10008.5.1.4.1.1.88.59^^1.2.840.10008.2.6.1”).");
		notif.setLocation(location);
		notif.setIdentifiant("chxds-DocumentEntryDicomConstraints-CHFormatCodeIsDICOMManifestConstraint");
		
		diagnostic.add(notif);
	}

}
