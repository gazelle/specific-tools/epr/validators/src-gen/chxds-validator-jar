package net.ihe.gazelle.chxds.validator.chxdsdsdr;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExternalIdentifierType;
import net.ihe.gazelle.rim.RegistryPackageType;



public class CHXDSDSDRPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHXDSDSDRPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.rim.ExtrinsicObjectType){
			net.ihe.gazelle.rim.ExtrinsicObjectType aClass = ( net.ihe.gazelle.rim.ExtrinsicObjectType)obj;
			CHXDSDRDocumentEntryValidator._validateCHXDSDRDocumentEntry(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.rim.RegistryObjectListType){
			net.ihe.gazelle.rim.RegistryObjectListType aClass = ( net.ihe.gazelle.rim.RegistryObjectListType)obj;
			CHXDSDSDRConstraintValidator._validateCHXDSDSDRConstraint(aClass, location, diagnostic);
		}
	
	}

}

