package net.ihe.gazelle.chxds.validator.chxds;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.LocalizedStringType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;
import net.ihe.gazelle.rim.ExtrinsicObjectType;


public class CHXDSPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHXDSPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.rim.AdhocQueryType){
			net.ihe.gazelle.rim.AdhocQueryType aClass = ( net.ihe.gazelle.rim.AdhocQueryType)obj;
			AdhocQueryConstraintsValidator._validateAdhocQueryConstraints(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.rim.ClassificationType){
			net.ihe.gazelle.rim.ClassificationType aClass = ( net.ihe.gazelle.rim.ClassificationType)obj;
			AuthorSubmissionSetValidator._validateAuthorSubmissionSet(aClass, location, diagnostic);
		}
		if (obj instanceof net.ihe.gazelle.rim.ExtrinsicObjectType){
			net.ihe.gazelle.rim.ExtrinsicObjectType aClass = ( net.ihe.gazelle.rim.ExtrinsicObjectType)obj;
			DocumentEntryDicomConstraintsValidator._validateDocumentEntryDicomConstraints(aClass, location, diagnostic);
			DocumentEntryOriginalProviderRoleValidator._validateDocumentEntryOriginalProviderRole(aClass, location, diagnostic);
		}
	
	}

}

