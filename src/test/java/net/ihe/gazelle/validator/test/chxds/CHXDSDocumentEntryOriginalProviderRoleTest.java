package net.ihe.gazelle.validator.test.chxds;

import org.junit.Assert;
import org.junit.Test;

public class CHXDSDocumentEntryOriginalProviderRoleTest {

    XDSProvideAndRegisterTestUtil testExecutor = new XDSProvideAndRegisterTestUtil();

    @Test
    public void test_ok_constraint_documententry_originalproviderrole_present() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_PNR_DocumentEntryOriginalProviderRole_OK.xml",
                        "chxds-DocumentEntryOriginalProviderRole-CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet"));
    }

    @Test
    public void test_ok_constraint_documententry_originalproviderrole_not_present() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_PNR_DocumentEntryOriginalProviderRole_KO.xml",
                        "chxds-DocumentEntryOriginalProviderRole-CHXDS_DocumentEntry_OriginalProviderRole_In_ValueSet"));
    }

    @Test
    public void test_ok_constraint_documententry_originalproviderrole_notCX() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_PNR_DocumentEntryOriginalProviderRole_notCX_KO.xml",
                        "chxds-DocumentEntryOriginalProviderRole-CHXDS_DocumentEntry_OriginalProviderRole_isCX"));
    }
}
