package net.ihe.gazelle.validator.test.chxdsdrdr;

import org.junit.Assert;
import org.junit.Test;

public class CHXDSITest {

    XDSTestUtil testExecutor = new XDSTestUtil();

    @Test
    public void test_ok_constraint_xua_SubmissionSetAuthor() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_PNR_OK.xml",
                        "chxdsdsdr-CHXDSDSDRConstraint-chxds_submissionSetAuthor"));
    }

    @Test
    public void test_ok_constraint_xua_DocumentEntryTitle() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_PNR_OK.xml",
                        "chxdsdsdr-CHXDSDRDocumentEntry-chxds_documentEntryTitle"));
    }

    @Test
    public void test_ko_constraint_xua_SubmissionSetAuthor() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_PNR_KO_SubSetAuthor.xml",
                        "chxdsdsdr-CHXDSDSDRConstraint-chxds_submissionSetAuthor"));
    }

    @Test
    public void test_ko_constraint_xua_DocumentEntryTitle() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_PNR_KO_DocEntryTitle.xml",
                        "chxdsdsdr-CHXDSDRDocumentEntry-chxds_documentEntryTitle"));
    }
}
