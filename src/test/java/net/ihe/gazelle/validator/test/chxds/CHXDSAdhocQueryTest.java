package net.ihe.gazelle.validator.test.chxds;

import org.junit.Assert;
import org.junit.Test;

public class CHXDSAdhocQueryTest {

    XDSAdhocQueryTestUtil testExecutor = new XDSAdhocQueryTestUtil();

    @Test
    public void test_ko_constraint_xua_xdsMetadataLevel() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_MetadataLevel_KO.xml",
                        "chxds-AdhocQueryConstraints-chxds_metadatalevel"));
    }

    @Test
    public void test_ok_constraint_xua_xdsMetadataLevel() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_MetadataLevel_OK.xml",
                        "chxds-AdhocQueryConstraints-chxds_metadatalevel"));
    }
}
