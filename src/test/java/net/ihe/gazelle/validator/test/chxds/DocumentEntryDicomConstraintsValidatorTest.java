package net.ihe.gazelle.validator.test.chxds;

import org.junit.Assert;
import org.junit.Test;

public class DocumentEntryDicomConstraintsValidatorTest {

    XDSProvideAndRegisterTestUtil testExecutor = new XDSProvideAndRegisterTestUtil();



    @Test
    public void test_ok_constraint_formatCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_PNRDICOM_OK.xml",
                        "chxds-DocumentEntryDicomConstraints-CHFormatCodeIsDICOMManifestConstraint"));
    }

    @Test
    public void test_ok_constraint_formatCodeName() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_PNRDICOM_formatCodeVSName_OK.xml",
                        "chxds-DocumentEntryDicomConstraints-CHFormatCodeIsDICOMManifestConstraint"));
    }

    @Test
    public void test_ok_constraint_formatCodeNameRAD68() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_PNRRAD68_formatCodeVSName_OK.xml",
                        "chxds-DocumentEntryDicomConstraints-CHFormatCodeIsDICOMManifestConstraint"));
    }

    @Test
    public void test_ko_constraint_formatCodeVSCodeSystem() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_PNRDICOM_formatCodeVSCodeSystem_KO.xml",
                        "chxds-DocumentEntryDicomConstraints-CHFormatCodeIsDICOMManifestConstraint"));
    }

    @Test
    public void test_ko_constraint_formatCodeVSCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_PNRDICOM_formatCodeVSCode_KO.xml",
                        "chxds-DocumentEntryDicomConstraints-CHFormatCodeIsDICOMManifestConstraint"));
    }
}
