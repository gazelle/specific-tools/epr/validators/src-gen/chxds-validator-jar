package net.ihe.gazelle.validator.test.chxds;


import net.ihe.gazelle.chxds.validator.chxds.CHXDSPackValidator;
import net.ihe.gazelle.common.StringMatcher;
import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;

import java.util.List;

public class XDSProvideAndRegisterTestUtil extends AbstractValidatorProvideAndRegisterRequest<ProvideAndRegisterDocumentSetRequestType> {

    static{
        StringMatcher.setValueSetProvider(new SVSConsumerCH());
    }

    @Override
    protected void validate(ProvideAndRegisterDocumentSetRequestType message,
                            List<Notification> notifications) {
        ProvideAndRegisterDocumentSetRequestType.validateByModule(message, "/AssertionType", new CHXDSPackValidator(), notifications);
    }

    @Override
    protected Class<ProvideAndRegisterDocumentSetRequestType> getMessageClass() {
        return ProvideAndRegisterDocumentSetRequestType.class;
    }
}