package net.ihe.gazelle.validator.test.chxds;


import net.ihe.gazelle.chxds.validator.chxds.CHXDSPackValidator;
import net.ihe.gazelle.query.AdhocQueryRequestType;
import net.ihe.gazelle.validation.Notification;

import java.util.List;

public class XDSTestUtil extends AbstractValidator<AdhocQueryRequestType> {

    @Override
    protected void validate(AdhocQueryRequestType message,
                            List<Notification> notifications) {
        AdhocQueryRequestType.validateByModule(message, "/AssertionType", new CHXDSPackValidator(), notifications);
    }

    @Override
    protected Class<AdhocQueryRequestType> getMessageClass() {
        return AdhocQueryRequestType.class;
    }
}