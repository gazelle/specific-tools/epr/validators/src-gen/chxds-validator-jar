package net.ihe.gazelle.validator.test.chxds;

import net.ihe.gazelle.common.Concept;
import net.ihe.gazelle.common.ValueSetProvider;
import net.ihe.gazelle.common.XmlUtil;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SVSConsumerCH implements ValueSetProvider {
    /**
     *
     * @param valueSetId
     * @param lang
     * @return
     */
    public List<Concept> getConceptsListFromValueSet(String valueSetId, String lang)
    {
        if (valueSetId == null || valueSetId.isEmpty())
        {
            return null;
        }
        String svsRepository = "https://ehealthsuisse.ihe-europe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
        //String svsRepository = "https://bcu-indus.ihe-europe.net/SVSSimulator/rest/RetrieveValueSetForSimulator";
        ClientRequest request = new ClientRequest(svsRepository);
        request.queryParameter("id", valueSetId);
        if (lang != null && !lang.isEmpty())
        {
            request.queryParameter("lang", lang);
        }
        try {
            ClientResponse<String> response = request.get(String.class);
            if (response.getStatus() == 200)
            {
                String xmlContent = response.getEntity();
                Document document = XmlUtil.parse(xmlContent);
                NodeList concepts = document.getElementsByTagName("Concept");
                if (concepts.getLength() > 0)
                {
                    List<Concept> conceptsList = new ArrayList<Concept>();
                    for(int index = 0; index < concepts.getLength(); index ++)
                    {
                        Node conceptNode = concepts.item(index);
                        NamedNodeMap attributes = conceptNode.getAttributes();
                        conceptsList.add(new Concept(attributes.getNamedItem("code").getTextContent(),
                                attributes.getNamedItem("displayName").getTextContent(),
                                attributes.getNamedItem("codeSystem").getTextContent(), null));
                    }
                    Collections.sort(conceptsList);
                    return conceptsList;
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
