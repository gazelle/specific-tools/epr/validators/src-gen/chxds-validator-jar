package net.ihe.gazelle.validator.test.chxdsdrdr;


import net.ihe.gazelle.chxds.validator.chxdsdsdr.CHXDSDSDRPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;

import java.util.List;

public class XDSTestUtil extends AbstractValidator<ProvideAndRegisterDocumentSetRequestType> {

    @Override
    protected void validate(ProvideAndRegisterDocumentSetRequestType message,
                            List<Notification> notifications) {
        ProvideAndRegisterDocumentSetRequestType.validateByModule(message, "/AssertionType", new CHXDSDSDRPackValidator(), notifications);
    }

    @Override
    protected Class<ProvideAndRegisterDocumentSetRequestType> getMessageClass() {
        return ProvideAndRegisterDocumentSetRequestType.class;
    }
}