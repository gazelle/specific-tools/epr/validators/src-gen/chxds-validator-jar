package net.ihe.gazelle.validator.test.chxds;

import org.junit.Assert;
import org.junit.Test;

public class CHXDSSubmissionSetTest {

    XDSProvideAndRegisterTestUtil testExecutor = new XDSProvideAndRegisterTestUtil();

    @Test
    public void test_ok_constraint_submissionset_authorRole_not_present() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_PNR_MetadataSubmissionSetAuthorRoleWithoutAuthorRole_OK.xml",
                        "chxds-AuthorSubmissionSet-CHXDS_Author_AuthorRole_In_ValueSet"));
    }

    @Test
    public void test_ko_constraint_submissionset_authorRole_present_wrong_vscode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_PNR_MetadataSubmissionSetAuthorRoleWrongValueSetCode_KO.xml",
                        "chxds-AuthorSubmissionSet-CHXDS_Author_AuthorRole_In_ValueSet"));
    }

    @Test
    public void test_ko_constraint_submissionset_authorRole_present_wrong_vscodesystem() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDS_PNR_MetadataSubmissionSetAuthorRoleWrongValueSetCodeSystem_KO.xml",
                        "chxds-AuthorSubmissionSet-CHXDS_Author_AuthorRole_In_ValueSet"));
    }

    @Test
    public void test_ok_constraint_submissionset_authorRole_present() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDS_PNR_MetadataSubmissionSetAuthorRole_OK.xml",
                        "chxds-AuthorSubmissionSet-CHXDS_Author_AuthorRole_In_ValueSet"));
    }
}
